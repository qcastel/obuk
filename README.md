# OpenBanking UK Profiles for API Security  #

The FAPI WG (http://openid.net/wg/fapi) and members of UK OpenBanking (http://openbanking.org.uk) are working to create standardized security profiles for the purposes of securing banking APIs. In general, the profiles in this repository build on the FAPI specifications and others.

### What is this repository for? ###

* OAuth-based high assurance profiles for the purposes of protecting banking APIs
    * Registration of Software Clients
    * Authorization Requests to Financial Institutions (known as ASPSPs)
* Working storage, version control and bug tracking for security profiles  
* Implementation Guides
* Useful links:
    * [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* More information to come - for now, see http://openbanking.org.uk

### Contribution guidelines ###
* This repository is open to FAPI members
* You MUST Execute Contributor Agreement either by
    * Electronic Signature and notify openid-specs-fapi-owner@lists.openid.net; OR
    * print this PDF and fill, sign, scan, and send it to openid-specs-fapi-owner@lists.openid.net.
* You MUST file an issue to the issue tracker before contributing any code
* You MUST test that the contributed code compiles without error

### Who do I talk to? ###

* Send inquiries to the FAPI owners mailing list: openid-specs-fapi-owner@lists.openid.net
