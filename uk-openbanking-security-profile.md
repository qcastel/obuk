# UK Open Banking OIDC Security Profile
## Introduction

This document is based on the OpenID Foundations Financial API Read+Write specification document, [FAPI-RW],  which in turn is based on the Read only specification document. The OpenBanking profile will further shape these two base profiles in some cases tightening restrictions and in others loosening requirements using keywords. Please see the introduction to [FAPI-RW] for a general introduction to the aims of this document.

The OpenBanking Profile detailed below outlines the differences between the [FAPI-RW] with clauses and provisions necessary to reduce delivery risk for ASPSPs OP.

### Notational Conventions
The key words "shall", "shall not", "should", "should not", "may", and "can" in this document are to be interpreted as described in ISO Directive Part 2. These key words are not used as dictionary terms such that any occurence of them shall be interpreted as key words and are not to be interpreted with their natural language meanings.

## 2. Normative References

The following referenced documents are strongly recommended to be used in conjunction with this document. For dated references, only the edition cited applies. For undated references, the latest edition of the referenced document (including any amendments) applies.

[FAPI] - FAPI ReadOnly profile
[FAPI]: http://openid.net/specs/openid-financial-api-part-1.html

[FAPI-RW] - FAPI Read+Write profile
[FAPI-RW]: http://openid.net/specs/openid-financial-api-part-2.html

[OIDC] - OpenID Connect Core 1.0 incorporating errata set 1
[OIDC]: http://openid.net/specs/openid-connect-core-1_0.html

[MTLS] - Mutual TLS Profile for OAuth 2.0
[MTLS]: https://tools.ietf.org/html/draft-ietf-oauth-mtls-03

## 4. Symbols and Abbreviated terms

**ASPSP** - Account Servicing Payment Services Provider (commonly a bank)

**PSU** - Payment Service User (a natural or legal person making use of a payment service as a payee, payer or both)

## 5. Read and Write API Security Profile
### 5.2  OB API Security Provisions
#### 5.2.1 Introduction
Open Banking's API Profile does not distinguish between the security requirements from a technical level between "read" and "write" resources. The security requirements for accessing PSU resources held at ASPSPs requires more protection level than a basic RFC6749 supports.

As a profile of The OAuth 2.0 Authorization Framework, this document mandates the following to the OpenBanking Financial APIs.


#### 5.2.2 Authorization Server
The Authorization Server

   1. shall support confidential clients;
   1. may support public clients;
   1. shall support user authentication at appropriate level as defined in PSD2;
   1. shall secure its token endpoint using mutually authenticated TLS;
   1. should require the `response_type` values `code id_token`; (weakened form of [FAPI-RW] 5.2.2.3)
   1. may allow the `response_type` value `code` (as an interim measure if not yet able to support `code id_token`);
   1. shall authenticate the confidential client at the Token Endpoint using one of the following methods:
    1. `tls_client_auth` as per [MTLS] (Recommended); or
    2. `client_secret_basic` or `client_secret_post` provided the client identifier matches the client identifier bound to the underlying mutually authenticated TLS session (Allowed); or
    3. `private_key_jwt` or 'client_secret_jwt` (Recommended);
   1. shall issue an ID Token in the token response when openid was included in the requested scope as in Section 3.1.3.3 of OIDC with its sub value corresponding to the "Intent Ticket ID"  and optional acr value in ID Token.
   1. may support refresh tokens.

Further, if it wishes to provide the authenticated user's identifier to the client in the token response, the authorization server

   1. shall issue an ID Token in the token response when openid was included in the requested scope as in Section 3.1.3.3 of [OIDC] with its sub value corresponding to the authenticated user and mandatory acr value in ID Token.
   1. shall support Request Objects passed by value as in clause 6.3 of OIDC.

#### 5.2.3 Public Client
OpenBanking OPs can support Public Clients at their discretion.

Should an OP wish to support public clients all provisions of [FAPI-RW] will be adhered too with exceptions below;

   1. shall request user authentication at appropriate level as defined in PSD2 be that LoA 3 or 2 or other.

#### 5.2.4 Confidential Client
A Confidential Client

   1. may use separate and distinct Redirect URI for each Authorization Server that it talks to;
   1. shall accept signed ID Tokens; 

## 6. Accessing Protected Resources
### 6.2 Access provisions
#### 6.2.1 Protected resources provisions
The resource server with the FAPI endpoints

   1. shall mandate mutually authenticated TLS;
   1. shall verify that the client identifier bound to the underlying mutually authenticated TLS transport session matches the client that the access token was issued to;

### 6.2.2 Client provisions
The confidential client supporting this document

   1. shall use mutually authenticated TLS;
   1. shall supply the unique identifier of the financial institution (as assigned by OpenBanking) as defined by [FAPI] clause 6.2.2.3
   1. shall supply the last time the customer logged into the client as defined by [FAPI] clause 6.2.2.4; and
   1. shall supply the customer's IP address if this data is available as defined by [FAPI] clause 6.2.2.5;

Further, the client

   1. may supply the customers authentication context reference (ACR) or applicable in the x-fapi-customer-acr header, e.g., x-fapi-customer-acr; 

## 7. Request object endpoint
### 7.1 Introduction

   1. OPs should support `request_uri`, OIDC Request Object by Reference.
   1. OPs shall support Request Objects passed by value as in clause 6.3 of OIDC.

## 8. Security Considerations
### 8.1 TLS Considerations

The TLS considerations in FAPI Part 1 section 7.1 shall be followed.

### 8.2 Message source authentication failure and message integrity protection failure

It is not mandated that the Authorization request and response are authenticated. To provide message source authentication and integrity protection:

   1. Authorization servers should support the Request Object Endpoint as per [FAPI-RW] clause 7
   1. Clients should Use request_uri (obtained from the Authorization Server) in the Authorization request as per [FAPI-RW] clause 5.2.2.1
   1. Authorization servers should return an ID token as a detached signature ot the authorization response as per [FAPI-RW] part 2 clause 5.2.2.3.

### 8.3 Message containment failure

The Message containment failure considerations in [FAPI] section 7.4 shall be followed.

### 8.5 TLS Considerations

The TLS considerations in [FAPI-RW] section 8.5 shall be followed.
